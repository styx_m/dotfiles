#!/bin/bash

# https://bbs.archlinux.org/viewtopic.php?id=194284

maildirs="$HOME/Mail/*/INBOX/new/"
ml="$(find $maildirs -type f | wc -l)"

if (( ml > 0 )); then
  echo "$ml"
else
  echo "0"
fi
