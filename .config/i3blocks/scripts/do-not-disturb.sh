
dnd_on=$(dunstctl is-paused)

if $dnd_on
then
	echo "<span foreground=\"red\">DnD</span>";
else
	echo "<span foreground=\"green\">!DnD</span>";
fi
