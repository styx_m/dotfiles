strength_percent=$(awk 'NR==3 {print $3 "00"}' /proc/net/wireless)

if  [ $(echo "$strength_percent > 0" | bc) -eq 1 ]
then
	echo $strength_percent"%"
else
	echo "<span foreground=\"red\">Disconnected</span>";
fi
