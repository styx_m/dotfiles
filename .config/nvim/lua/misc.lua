
-- note: plugin-specific autocmds are in plugin-settings.vim


-- augroup Emmet
--     au FileType html,gohtmltmpl imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")
-- augroup END

-- Auto-save folds
vim.cmd [[
	" augroup SaveFolds
	" au BufWrite,VimLeave *.txt mkview
	" au BufRead           *.txt silent loadview
	" augroup END

	augroup code
	autocmd FileType !text,!markdown set expandtab
	augroup END

	augroup VIMRC
	autocmd!
	autocmd BufLeave *.css,*.scss                  normal! mC
	autocmd BufLeave *.html                        normal! mH
	autocmd BufLeave *.js,*.ts,*.jsx,*.tsx         normal! mJ
	augroup END

	augroup FiletypeGroup
	autocmd!
	au BufNewFile,BufRead *.jsx set filetype=javascript.jsx
	au BufNewFile,BufRead *.tsx set filetype=typescript.tsx
	augroup END
]]

-- go to last cursor position on buffer open
vim.cmd [[
	autocmd BufReadPost * if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif 
]]

vim.cmd("command! -nargs=1 MoveMentionToBottom g/<args>/normal ddGp")

