local nnoremap = require('utils').nnoremap

require('bufferline').setup{
    options = {
        show_buffer_close_icons = false,
        enforce_regular_tabs = true
    },
    highlights = {
        tab = {
            guibg = {
                attribute = 'bg',
                highlight = 'TabLine'
            }
        },
        buffer_selected = {
            gui = 'none'
        }
    },
}
nnoremap('<Leader>gb', '<cmd>BufferLinePick<CR>')

