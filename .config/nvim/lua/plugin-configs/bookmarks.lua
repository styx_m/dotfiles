local nnoremap = require('utils').nnoremap

vim.api.nvim_set_var('bookmark_no_default_key_mappings', 1)

nnoremap('<Leader>ba', '<Plug>BookmarkAnnotate')
nnoremap('<Leader>bs', '<Plug>BookmarkShowAll')
nnoremap('<Leader>bc', '<Plug>BookmarkClear')

