local lsp_installer = require'nvim-lsp-installer'

-- Register a handler that will be called for all installed servers.
-- Alternatively, you may also register handlers on specific server instances instead (see example below).
lsp_installer.on_server_ready(function(server)

    local opts = {}

    opts.settings = {
        Lua = {
            diagnostics = {
                globals = {"vim", "use", "describe", "it"}
                -- disable = {"lowercase-global"}
            }
        }
    }

    -- https://github.com/ray-x/lsp_signature.nvim/issues/1#issuecomment-865137902
    require'lsp_signature'.on_attach({
        bind = false,
        use_lspsaga = true,
        floating_window = true,
        fix_pos = true,
        hint_enable = true,
        hi_parameter = "Search",
        handler_opts = {
            "shadow"
        }
    })

    opts.on_attach = require 'illuminate'.on_attach

    opts.capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

    print(server.name)
    if server.name == "sumneko_lua" then
        server:setup(require('lua-dev').setup {
            library = {
                vimruntime = true,
                types = true,
                plugins = false,
            },

            lspconfig = {
                settings = {
                    Lua = {
                        telemetry = {
                            enable = false,
                        },
                        workspace = {
                            preloadFileSize = 180
                        }
                    },
                },
            }
        })

        return
    elseif server.name == "rust_analyzer" then
        -- Initialize the LSP via rust-tools instead
        require("rust-tools").setup {
            -- The "server" property provided in rust-tools setup function are the
            -- settings rust-tools will provide to lspconfig during init.            --
            -- We merge the necessary settings from nvim-lsp-installer (server:get_default_options())
            -- with the user's own settings (opts).
            server = vim.tbl_deep_extend("force", server:get_default_options(), opts),
        }
        server:attach_buffers()
    else
        -- This setup() function is exactly the same as lspconfig's setup function.
        -- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
        server:setup(opts)
    end
end)

