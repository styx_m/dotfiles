local add_prefix = require('utils').add_prefix
local remove_prefix = require('utils').remove_prefix
local vnoremap = require('utils').vnoremap
local nnoremap = require('utils').nnoremap

add_prefix('<lspleader>r', 'refactor');

require('refactoring').setup({
    -- prompt for return type
    prompt_func_return_type = {
        go = true,
        cpp = true,
        c = true,
        java = true,
    },
    -- prompt for function parameters
    prompt_func_param_type = {
        go = true,
        cpp = true,
        c = true,
        java = true,
    },
})

vnoremap(
    "r",
    ":lua require('refactoring').select_refactor()<CR>",
    { noremap = true, silent = true, expr = false }
)

-- Remap in normal mode and passing { normal = true } will automatically find the variable under the cursor and print it
nnoremap("v", ":lua require('refactoring').debug.print_var({ normal = true })<CR>", { noremap = true })
-- Remap in visual mode will print whatever is in the visual selection
vnoremap("v", ":lua require('refactoring').debug.print_var({})<CR>", { noremap = true })

-- Cleanup function: this remap should be made in normal mode
nnoremap("c", ":lua require('refactoring').debug.cleanup({})<CR>", { noremap = true })

remove_prefix();
