local nnoremap = require('utils').nnoremap

-- fasd is a separate (non-vim) command that allows one to cd/edit files from
-- anywhere based on frecency
vim.cmd [[
	function! FasdEIntegration(fileToOpen)

	  let l:target = system("fasd -f -l -1 " . a:fileToOpen)
	  execute "edit " . l:target
	endfunction

	function! FasdZIntegration(targetDirectory)

	  let l:target = system("fasd -d -l -1 " . a:targetDirectory)
	  execute "cd " . l:target
	endfunction

	" usage: Fd <file name to open>
	command! -nargs=1 Fde call FasdEIntegration(<f-args>)
	command! -nargs=1 Fdz call FasdZIntegration(<f-args>)
]]

nnoremap('<localleader>e', ':Fde ')

