vim.opt.termguicolors = true

vim.api.nvim_set_var('tokyonight_style',  'night') -- available: night, storm
vim.api.nvim_set_var('tokyonight_enable_italic',  1)

