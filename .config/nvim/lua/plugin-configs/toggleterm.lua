local nnoremap = require('utils').nnoremap
local tnoremap = require('utils').tnoremap
local inoremap = require('utils').inoremap

require("toggleterm").setup{}

nnoremap('<leader>tt', ':ToggleTerm<CR>')
tnoremap('<leader>tt', ':ToggleTerm<CR>')
inoremap('<leader>tt', '<C-\\><C-n>:ToggleTerm<CR>')
