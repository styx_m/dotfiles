
" Purpose: if there's a line like the two below somewhere in the file after the cursor, jump to that file, and if there's an anchor in the link (e.g, the second example), jump to the text of the anchor
" [December 19, 2018, Wednesday](test.md)
" [December 19, 2018, Wednesday](test.md#January 1, 2018, Friday)

" @param anchorStart - if anchor doesn't matched exactly where you want to jump to, can append regex to start of anchor
" @param anchorEnd - same as anchorStart, but regex appended to end of anchor text
function! JumpToLink(anchorStart, anchorEnd) abort

  call Jump(a:anchorStart, a:anchorEnd, '\(](\)\@<=\(.\+\)\(#\|)\)\@=', '\(](\)\@<=\(\f\+\))\@=', '\(](\)\@<=\(\f\+\)\(#\)\@=', '\(.\+\..\+#\)\@<=.\+\()\)\@=')

  " let s:charUnderCursor=getline('.')[col('.')-1]
  " let s:cursorPossiblyOnFilename=len(matchstr(s:charUnderCursor, '\f')) > 0

  " if s:cursorPossiblyOnFilename
  "   let s:cursorPos=getcurpos()
  "   normal! F 
  " endif

  " let s:lineOfLink=search('\(](\)\@<=\(.\+\)\(#\|)\)\@=', 'ncW', line("w$"))

  " if s:lineOfLink == 0
  "   echo "JumpToLink: No link found"
  "   return
  " endif

  " " if there's a link on the same line as cursor, use it (so don't go backwards
  " " to a link on the same line), else just search forward from the start of
  " " lines
  " if s:lineOfLink == s:cursorPos[1]
  "   let s:cursorCol=s:cursorPos[2]
  " else
  "   let s:cursorCol=0
  " endif

  " let s:line=getline(s:lineOfLink)[s:cursorCol:]

  " let s:anchorInLink=matchstr(s:line, '\f\.\f\+#\f')

  " if len(s:anchorInLink) == 0
  "   let s:linkFilename=matchstr(s:line, '\(](\)\@<=\(\f\+\))\@=')
  "   let s:linkAnchor=''
  " else
  "   let s:linkFilename=matchstr(s:line, '\(](\)\@<=\(\f\+\)\(#\)\@=')
  "   let s:linkAnchor=matchstr(s:line, '\('.s:linkFilename.'#\)\@<=\f\+\()\)\@=')
  " endif

  " if s:linkFilename[0] != "/"
  "   let s:currentBufferPath = expand("%:p:h") . "/"
  " else
  "   let s:currentBufferPath = ""
  " endif

  " echo s:cursorPos
  " call cursor(s:cursorPos[1], s:cursorPos[2])

  " execute "edit " . s:currentBufferPath . s:linkFilename

  " let s:anchorPos=searchpos(a:anchorStart . s:linkAnchor . a:anchorEnd, 'ncW')
  " call cursor(s:anchorPos[0], s:anchorPos[1])

endfunction

