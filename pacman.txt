redshift
firejail
keepassxc
the_silver_searcher
nodejs
npm
nextcloud-client
nemo
pandoc
polkit
screenfetch
tmux
tree
ufw
cups
scp
fzf
mupdf
acpi
lynx
clamav
rofi
qt5ct
qtconfig-qt5
arc-kde
kvantum-theme-arc
gnome-keyring
secret-tools
khard
neomutt
offlineimap
etckeeper
# wicd
# wicd-gtk
# xtrlock
task # taskwarrior
wireguard-tools
jq
fasd
grim; slurp; zenity
light
borg
cronie
typescript
ttf-fira-mono
zathura
zathura-pdf-mupdf
perl-anyevent-i3 # required for my layout management script
zsh-syntax-highlighting
zsh-autosuggestions
